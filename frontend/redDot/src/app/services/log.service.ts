import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class LogService {
    static logId: string | null = null; //64258ef4cee76887340c90b3

    constructor() { } 

    public static hasLogged(id: string) {
        this.logId = id;
    }

    public static deconnect() {
        this.logId = null;
    }

    public static isLogged() : Boolean {
        return this.logId != null;
    }
}