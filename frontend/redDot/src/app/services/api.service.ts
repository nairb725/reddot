import { Injectable } from '@angular/core';
import axios from 'axios';

axios.interceptors.response.use(
    (response) => {
        return response.data
    },
    (error) => {
        console.log(error);
        switch(error.response.status){
            case 400:
                return false;
            default:
                return error.response.data
        }
    }
)

@Injectable({
  providedIn: 'root'
})
export class ApiService {

    BASE_URL = "http://localhost:3000";

    constructor () { }

    public signIn(body: Object) {
        return axios.post(this.BASE_URL + "/users/connect", body);
    }

    public signUp(body: Object) {
        return axios.post(this.BASE_URL + "/users/create", body)
    }

    public getLastMessageUser(idUser: string){
        return axios.get(this.BASE_URL + "/users/" + idUser + "/lastMessages");
    }


    public createSub(data: Object){
        return axios.post(this.BASE_URL + "/subs/create", data);
    }

    public getSub(idSub: string) {
        return axios.get(this.BASE_URL + "/subs/" + idSub);
    }

    public joinSub(idSub: string, idUser: string){
        return axios.post(this.BASE_URL + "/subs/" + idSub + "/join", { id_user: idUser });
    }

    public searchSub(keyWord: string){
        return axios.get(this.BASE_URL + "/subs/search?keyWords=" + keyWord);
    }

    
    public createPost(data: Object, idSub: string) {
        return axios.post(this.BASE_URL + "/subs/" + idSub + "/messages", data)
    }
}