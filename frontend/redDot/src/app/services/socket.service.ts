import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';  

@Injectable({
	providedIn: 'root'
})
export class SocketService {
	constructor(private socket: Socket) { }

    fetchPosts() {
        this.socket.emit('fetchPosts');
    }

    onFetchPosts() {
        return this.socket.fromEvent('fetchPosts');
    }
}