import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../services/api.service';
import { LogService } from '../services/log.service';
import { SocketService } from 'src/app/services/socket.service';  

@Component({
  selector: 'app-sub-page',
  templateUrl: './sub-page.component.html',
  styleUrls: ['./sub-page.component.scss']
})
export class SubCardsComponent {
    public subId;
    public sub: any;

    constructor (public api:ApiService, public log: LogService, public route: ActivatedRoute, private socketService: SocketService ) {
        const { id } = route.snapshot.params; //642413047e798e80964f1422
        this.subId = id;
        this.getSubData();
        this.isJoined();
		this.socketService.fetchPosts();
		this.socketService.onFetchPosts().subscribe((data: any) => {
            if(this.sub) this.sub.posts = data; // to make sure sub is load correctly before pushing other post
        });
    }

    async getSubData() {
        this.sub = await this.api.getSub(this.subId);
        if (this.sub.isSubscribed != null && this.sub.isSubscribed ){
            var element = <HTMLInputElement> document.getElementById("join_channel");
            element.disabled = true;
        }
    }

    async joinSub() {
        if (LogService.isLogged()){
           const res =  await this.api.joinSub(this.subId, LogService.logId!);
           if (res){
            var element = <HTMLInputElement> document.getElementById("join_channel");
            element.disabled = true;
           }
        } 
    }

    isLog() {
        return LogService.isLogged();
    }
    isJoined(){

    }
}
