import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ApiService } from '../services/api.service';
import { LogService } from '../services/log.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent {

  errorMsg : string = '';

  form: FormGroup = new FormGroup({
    name: new FormControl(""),
    password: new FormControl("")
  });

  constructor(public api: ApiService, public router: Router) {}

  async onSubmit() {
    const res: any = await this.api.signIn(this.form.value);

    if (!res) {
      this.errorMsg = 'Cet utilisateur n\'existe pas';
    } else {
      LogService.hasLogged(res._id);
      this.router.navigate(["/"]);
      //this.displayHeader.showHeader()
    }
  }
}
