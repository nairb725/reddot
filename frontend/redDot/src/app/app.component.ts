import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'redDot';

  constructor() {}

  public toggleDarkTheme() {
    document.body.classList.toggle('dark-theme');
  }
}