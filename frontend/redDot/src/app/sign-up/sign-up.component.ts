import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ApiService } from '../services/api.service';
import { Router } from '@angular/router';
import { LogService } from '../services/log.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent {

  form: FormGroup = new FormGroup({
    name: new FormControl(""),
    email: new FormControl(""),
    password: new FormControl(""),
  });

  constructor(public api:ApiService, public router: Router, public log: LogService) {}

  async onSubmit() {
    const res: any = await this.api.signUp(this.form.value);

    if (!res) {
      console.log("can't create user");
    } else {
      LogService.hasLogged(res._id)
      this.router.navigate(["/"]);
      //this.displayHeader.showHeader()
    }
  }
}
