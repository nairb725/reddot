import { Component } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { LogService } from 'src/app/services/log.service';

@Component({
  selector: 'app-post-user-cards',
  templateUrl: './post-user-cards.component.html',
  styleUrls: ['./post-user-cards.component.scss']
})
export class PostUserCardsComponent {
  postList: any;

  constructor(public api: ApiService) { 
    this.getLastMessage();
  }

  private async getLastMessage() {
    if (LogService.isLogged()) this.postList = await this.api.getLastMessageUser(LogService.logId!);
    else this.postList = [
      {'user' : {'name': 'Anarion'}, 'sub' : {'title': 'jeux-vidéos'}, 'name' : 'Hogwarts Legacy, incroyable !'},
      {'user' : {'name': 'Infernaton'}, 'sub' : {'title': 'PolitiqueFR'}, 'name' : 'ONO !!'},
      {'user' : {'name': 'Cheshire'}, 'sub' : {'title': 'Cuisine'}, 'name' : 'C`\'est bon les pâtes au sucre.'},
      {'user' : {'name': 'Robinou'}, 'sub' : {'title': 'POE_FR'}, 'name' : 'Path of Exile est une douceur !'},
      {'user' : {'name': 'Phromaj'}, 'sub' : {'title': 'Société'}, 'name' : 'La calvasse, c\'est dur à vivre au quotidien...'},
      {'user' : {'name': 'Raykutan'}, 'sub' : {'title': 'Dev Web'}, 'name' : 'La Nasa j\'y rentre quand je veux t\'sais.'},
      {'user' : {'name': 'Jean-MichelMonNomEstTropLong'}, 'sub' : {'title': 'Dev Web'}, 'name' : 'La Nasa j\'y rentre quand je veux t\'sais.'},
    ];
  }

  public isLogged() {
    return LogService.isLogged()
  }
}
