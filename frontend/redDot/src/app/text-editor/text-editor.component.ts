import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ApiService } from '../services/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LogService } from '../services/log.service';

@Component({
  selector: 'app-text-editor',
  templateUrl: './text-editor.component.html',
  styleUrls: ['./text-editor.component.scss']
})
export class TextEditorComponent {

  form: FormGroup = new FormGroup({
    name: new FormControl(""),
    content: new FormControl("")
  });
  subId: string;
  
  publicationTitle = "";
  ckeditorContent = "";
  publishedContent = "";

  constructor(public api: ApiService, public route: ActivatedRoute, public router: Router) {
    const { id } = route.snapshot.params; //642413047e798e80964f1422
    this.subId = id;
  }

  async publish() {
    console.log(this.ckeditorContent);
    console.log(this.publicationTitle);
    // Store the content to the variable
    this.publishedContent = this.ckeditorContent;
    this.publicationTitle = this.publicationTitle;

    // Perform publish operation here
    const res = await this.api.createPost({
      name: this.form.value.name,
      content: this.publishedContent,
      idUser: LogService.logId!
    }, this.subId)

    // Reset the ckeditorContent after publishing
    if (res){
      this.ckeditorContent = "";
      this.publicationTitle = "";
      this.router.navigate(["/sub/" + this.subId]);
    } else {
      console.log("Error on Posting message");
    }
  }
}
