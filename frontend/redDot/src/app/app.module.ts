import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './home-page/home-page.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { TextEditorComponent } from './text-editor/text-editor.component';
import { HttpClientModule} from '@angular/common/http';
import { CKEditorModule } from 'ng2-ckeditor';
import { FormsModule } from '@angular/forms';
import { PostUserCardsComponent } from './widgets/post-user-cards/post-user-cards.component';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { SubCardsComponent } from './sub-page/sub-page.component';
import { SubFormPageComponent } from './sub-form-page/sub-form-page.component';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';

const config: SocketIoConfig = {
	url: "http://localhost:3000", // socket server url;
	options: {
		transports: ['websocket']
	}
}

const routes : Routes = [
  { path: '', component: PostUserCardsComponent },
  { path: 'signUp', component: SignUpComponent },
  { path: 'signIn', component: SignInComponent },
  { path: 'sub/:id', component: SubCardsComponent },
  { path: 'sub/:id/newPost', component: TextEditorComponent },
  { path: 'subForm', component: SubFormPageComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    SignUpComponent,
    SignInComponent,
    TextEditorComponent,
    PostUserCardsComponent,
    SubCardsComponent,
    SubFormPageComponent,
  ],
  imports: [
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule, 
    FormsModule,
    CKEditorModule,
    RouterModule.forRoot(routes),
    SocketIoModule.forRoot(config),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
