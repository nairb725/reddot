import { Component } from '@angular/core';
import { LogService } from '../services/log.service';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent {

  results: any;
  search: string;

  constructor(public api: ApiService) {
    this.search = "";
  }

  public isLog() {
    return LogService.isLogged();
  }

  public async getSubKeyWord() {
    this.results = await this.api.searchSub(this.search);
    if (this.search == "") {
      this.results = [];
    }
  }

  public resetSearch(){
    this.search = "";
  }

  toggleDarkTheme(): void {
    document.body.classList.toggle('dark-theme');
  }
}


