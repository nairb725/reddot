import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ApiService } from '../services/api.service';
import { Router } from '@angular/router';
import { LogService } from '../services/log.service';

@Component({
  selector: 'app-sub-form-page',
  templateUrl: './sub-form-page.component.html',
  styleUrls: ['./sub-form-page.component.scss']
})
export class SubFormPageComponent {
  form: FormGroup = new FormGroup({
    title: new FormControl(""),
  });
  
  constructor(public api: ApiService, public router: Router) {}

  async onSubmit() {
    const res: any = await this.api.createSub(this.form.value);

    if (!res) {
      console.log("No user was found");
    } else {
      await this.api.joinSub(res._id, LogService.logId!);
      this.router.navigate(["/sub/"+  res._id]);
      //this.displayHeader.showHeader()
    }
  }
}
