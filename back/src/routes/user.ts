import express from 'express';
import { createUser, getUser, getUsers, getViewMessages, logIn } from '../controller/userController';
const router = express.Router();

router.get("/", getUsers);
router.post("/create", createUser);
router.post("/connect", logIn);
router.post("/:id", getUser);
router.get("/:idUser/lastMessages", getViewMessages);

export default router;