import express from 'express';
import { pushPost } from '../controller/postController';
const router = express.Router();
const subControllers = require("../controller/subController.js");

router.get("/", subControllers.getSubAll); //get all subGroupe
router.get("/search", subControllers.searchSub); 
router.get("/:idSub", subControllers.getSub); //get one subGroupe
router.post("/create", subControllers.createSub);
router.post("/:idSub/join", subControllers.joinSub);
router.delete("/:idSub", subControllers.deleteSub);

router.post("/:idSub/messages", pushPost);
router.get("/:idSub/messages/:idMessage", () => {});

export default router;