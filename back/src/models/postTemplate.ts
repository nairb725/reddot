import mongoose, { Schema } from "mongoose";

export interface IPost extends mongoose.Document {
    title: string;
    content: string;
    id_user: string;
    date: Date;
}

export const PostSchema = new mongoose.Schema({
    name: {type:String, required: true},
    content: {type:String, required: true},
    user: {type:Schema.Types.ObjectId, ref: "users", required: true},
    sub: {type:Schema.Types.ObjectId, ref: "sub", required: true},
    date: {type:Date, default: new Date()},
});

const PostModel = mongoose.model<IPost>('posts', PostSchema);
export default PostModel;
