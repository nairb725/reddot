import mongoose, { Schema } from "mongoose";

export interface ISub extends mongoose.Document {
    title: string;
    posts: Array<Schema.Types.ObjectId>;
}

export const SubSchema = new mongoose.Schema({
    title: {type: String, required: true},
    posts: {type:Array<Schema.Types.ObjectId>, default: []},
});

const SubModel = mongoose.model<ISub>('subs', SubSchema);
export default SubModel;
