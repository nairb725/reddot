import mongoose, { Schema } from "mongoose";

export interface IUser extends mongoose.Document {
    name: string;
    email: string;
    password: string;
    subscribed: Schema.Types.ObjectId[];
}

export const UserSchema = new mongoose.Schema({
    name: {type:String, required: true},
    email: {type:String, required: true},
    password: {type:String, required: true},
    subscribed: {type: Array<Schema.Types.ObjectId>, default:[]},
});

const UserModel = mongoose.model<IUser>('users', UserSchema);
export default UserModel;