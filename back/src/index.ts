import express, { Express, Request, Response } from "express";
import dotenv from 'dotenv';
import mongoose from "mongoose";
import cors from 'cors';
const { Server } = require("socket.io");

import userRoutes from './routes/user.js';
import { Socket } from "socket.io";

import subsRoutes from './routes/sub.js';

dotenv.config();

const app: Express = express();

const port = process.env.DB_PORT;

app.use(express.json());

app.use(cors())

app.get("/", (req: Request, res: Response) => {
  // res.sendFile(__dirname + '/index.html');
});

app.use("/users", userRoutes);
app.use("/subs", subsRoutes);

mongoose.connect(process.env.DB_CONN_STRING!)
  .then(() => {
    const server = app.listen(process.env.DB_PORT, () => console.log(`⚡️[server]: Server is running at http://localhost:${port}`));
    const io = new Server(server, {
      pingTimeout: 60000,
      cors: {
        origin: `http://localhost:4200`,
      }
    });

    io.on('connection', (socket: Socket) => {
      console.log('a user connected');

      socket.on('disconnect', () => {
        console.log('user disconnected');
      });
      socket.on('fetchPosts', (data) => {
        io.emit('fetchPosts', data);
      });
    });
  })
  .catch((error) => console.log(error));
