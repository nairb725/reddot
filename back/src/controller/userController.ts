import { Request, Response } from "express";
import { ObjectId } from "mongodb";
import { query } from "../handler/handleResponse";
import Post from "../models/postTemplate";
import User from "../models/userTemplate";

export const getUsers = (req: Request, res: Response) => query(res, async () => {
    const result = await User.find({});

    res.status(200).send(result);
});

export const getUser = async (req:Request, res: Response) => query(res, async () => {
    const { id } = req.body;

    if (!id) throw new Error("No ID define in body request");

    const user = await User.findById(new ObjectId(id));

    if (!user) throw new Error("No user with this ID");
    
    res.status(200).send(user)
});

export const createUser = (req: Request, res: Response) => query(res, async () => {
    const user = req.body as typeof User;

    const result = await User.create(user);

    if (!result) throw new Error("Failed to create a new user.");

    res.status(201).send(result);
});

export const logIn = (req: Request, res: Response) => query(res, async () => {
    const { password, name } = req.body;

    const result = await User.findOne({ password, name });

    result ? res.status(200).send(result)
           : res.status(400).send({"error": "No user find"});
});

export const getViewMessages = async (req: Request, res: Response) => query(res, async () => {
    const { idUser: id } = req.params;
    const user = await User.findOne({ _id: new ObjectId(id) });

    if (!user) throw new Error("IdUser not Found");

    const subscribedSub = await Post.find({sub: { $in: user!.subscribed }}).sort({date: 'asc'}).populate({
        path: 'user',
        model: 'users',
    }).populate({
        path: 'sub',
        model: 'subs',
    });
    
    res.status(200).send(subscribedSub);
});