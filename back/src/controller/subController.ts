import { Request, Response } from "express";
import { ObjectId, PushOperator } from "mongodb";
import { query } from "../handler/handleResponse";
import Sub from "../models/subTemplate";
import User from "../models/userTemplate";

export const getSub = (req: Request, res: Response) => query(res, async () => {
  const { idSub } = req.params;
  const { logIdUser } = req.query;
  let results: any = await Sub.findOne({ _id: new ObjectId(idSub) }).populate({
    path: 'posts',
    model: 'posts',
    populate: {
      path: 'user',
      model: 'users',
    }
  });

  if (!results) throw new Error("No sub were found");

  if (logIdUser){
    const user = await User.findById(logIdUser);
    results!.isSubscribed = user?.subscribed.includes(results!._id);
  }
  res.status(200).send(results);
});

export const getSubAll = (req: Request, res: Response) => query(res, async () => {
  const results = await Sub.find({});
  res.status(200).send(results);
});

export const deleteSub = (req: Request, res: Response) => query(res, async () => {
  const { idSub } = req.params;
  const results = await Sub.deleteOne({ _id:new ObjectId(idSub) })
  res.status(200).send(results);
});

export const createSub = (req: Request, res: Response) => query(res, async () => {
  const { title }  = req.body;
  const results = await Sub.create({ title });
  res.status(200).send(results);
});

export const joinSub = (req: Request, res: Response) => query(res, async () => {
  const { id_user } = req.body;
  const { idSub } = req.params;
  
  const results = await User.updateOne(
    { _id: new ObjectId(id_user) }, 
    { 
      $push: { subscribed: new ObjectId(idSub) } as unknown as PushOperator<Document>
    }
  );
  res.status(200).send(results);
});

export const searchSub = (req: Request, res: Response) => query(res, async () => {
  const { keyWords } = req.query;

  const results = await Sub.find({title: { $regex: keyWords, $options: "i"}})
  res.status(200).send(results);
});