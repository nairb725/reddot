import { ObjectId, PushOperator } from "mongodb";
import Post from "../models/postTemplate";
import { Request, Response } from "express";
import Sub from "../models/subTemplate";
import { query } from "../handler/handleResponse";

export const pushPost = (req: Request, res: Response) => query(res, async () => {
  const { idSub : sub } = req.params;
  const { name, content, idUser: user} = req.body;

  const postDb = await Post.create({ name, content, user, sub });

  if (!postDb) throw new Error("Can't created post.");
  
  const results = await Sub.findOneAndUpdate(
    { _id: new ObjectId(sub) }, 
    { $push: { posts: postDb._id } }
  );
  res.status(200).send(results);
});
