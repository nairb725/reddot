import { Response } from "express";

export const query = async (res: Response, _function: Function) => {
    try {
        _function()
    } catch (error){
        res.send(500).send({"error": error});
    }
}